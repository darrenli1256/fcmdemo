require('dotenv').config()
let envParams = process.env

const express = require('express')
const app = express()
const port = envParams.PORT
const path = require('path')

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})

app.get('/getFirebaseInfo', (req, res) => {
  let firebaseConfig = {
    apiKey: envParams.apiKey,
    // authDomain: envParams.authDomain,
    projectId: envParams.projectId,
    // storageBucket: envParams.storageBucket,
    messagingSenderId: envParams.messagingSenderId,
    appId: envParams.appId,
    // measurementId: envParams.measurementId
  };
  let vapidKey = envParams.vapidKey;
  let delivery = {
    firebaseConfig: firebaseConfig,
    vapidKey: vapidKey
  }
  res.json(delivery)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
