// Elements in the document
let MsgElem = document.getElementById("msg")
let TokenElem = document.getElementById("token")
let NotisElem = document.getElementById("notis")
let ErrElem = document.getElementById("err")

// Firebase config
let firebaseConfig;
let app;
let messaging;

fetchPack(
  "/getFirebaseInfo",
  "GET"
)
.then(result => {
  firebaseConfig = result.firebaseConfig;
  app = firebase.initializeApp(firebaseConfig);
  messaging = firebase.messaging();
  // messaging.usePublicVapidKey(result.vapidKey);
  return messaging.requestPermission()
})
.then(() => {
  MsgElem.innerHTML = "Notification permission granted.";
  return messaging.getToken()
})
.then((token) => {
  TokenElem.innerHTML = "Device token is : <br>" + token;
  messaging.onMessage((payload) => {
    console.log("onMessage: " + payload)
  })
})
.catch((err) => {
  ErrElem.innerHTML = ErrElem.innerHTML + "; " + err
  console.log("Unable to get permission to notify.", err)
})