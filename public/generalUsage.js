// ----- General Functions -----
const fetchPack = (endPoint, method, body = null) => {
	let option = {
		headers: {"Content-Type": "application/json"},
		method: method,
	};
	if (method == "POST") {
		option.body = JSON.stringify(body);
	}
	const fetching = fetch(endPoint, option)
		.then(response => {
			let message = response.json();
			return message;
		}); 
	return fetching;
};